from django.db import models
# Create your models here.

class Profesion(models.Model):
    nombre = models.CharField(max_length=20)
    anos = models.IntegerField()

    def __str__(self):
        return self.nombre


class Profesor(models.Model):
    profesion = models.ForeignKey(Profesion, null=True, blank=True, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=50)
    rut = models.CharField(max_length=15)
    apellidoPaterno = models.CharField(max_length=50)
    apellidoMaterno = models.CharField(max_length=50)
    edad = models.IntegerField()
    telefono = models.CharField(max_length=20)
    domicilio = models.TextField()
    fecha_registro = models.DateTimeField(auto_now=True)
    fecha_nacimiento = models.DateField()

    def __str__(self):
        return self.nombre
