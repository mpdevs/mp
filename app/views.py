from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# -----------  para nuestos modelos --------------------
from .models import Profesion, Profesor
from .forms import ProfesionForm

def principal(request):
    return render(request, 'app/principal.html', {})

def profesores(request):
    return render(request, 'app/profesores.html', {})

def quienessomos(request):
    return render(request, 'app/quienessomos.html', {})

def contacto(request):
    return render(request, 'app/contacto.html', {})

def enviado(request):
    return render(request, 'app/enviado.html', {})


# ======================== FUNCIONES =========================================
# -- codigo para crar el addProfesion para agregar registro
def inscripcion_profesion(request):
    if request.method == "POST":
        form = ProfesionForm(request.POST)
        if form.is_valid():
            model_instance = form.save(commit=False)
            model_instance.save()
            return redirect('/agregarProfesion')
    else:
        form = ProfesionForm()
        return render(request, 'app/inscripcion_profesion.html',
                      {'form': form})

def listar_profesiones(request):
    
    profesiones = Profesion.objects.all()
    
    return render(request,
        "app/listar_profesiones.html", {'profesiones': profesiones})

def listar_profesiones_full(request):
    
    profesiones = Profesion.objects.all()
  
    return render(request,
        "app/listar_profesiones_full.html", {'profesiones': profesiones})


def editar_profesion(request, profesion_id):
    
    instancia= Profesion.objects.get(id=profesion_id)
   
    form=  ProfesionForm(instance=instancia)
  
    if request.method=="POST":
        
        form= ProfesionForm(request.POST, instance=instancia)
        
        if form.is_valid():
           
            instancia= form.save(commit=False)
            
            instancia.save()
    return render(request, "app/editar_profesion.html",{'form':form})            

def borrar_profesion(request, profesion_id):    
    instacia= Profesion.objects.get(id=profesion_id)
    instacia.delete()
    return redirect('/')





# ======================== generics =========================================

class Profesion_Create(CreateView):
    model = Profesion
    form_class = ProfesionForm
    templates_name = 'app/inscripcion_profesion.html'
    success_url = reverse_lazy('profesion_crear')
