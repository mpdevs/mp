from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.principal),
    path('principal', views.principal),
    path('enviado', views.enviado),
    path('profesores', views.profesores),
    path('quienessomos', views.quienessomos),
    path('contacto', views.contacto),
    path('agregarprofesion',views.inscripcion_profesion),
    path('addProfesion', views.Profesion_Create.as_view(), name="profesion_crear"),
    path('listar_profesiones',views.listar_profesiones),
    path('editar_profesion/<int:profesion_id>', views.editar_profesion),
    path('borrar_profesion/<int:profesion_id>', views.borrar_profesion),
    path('listar_profesiones_full',views.listar_profesiones_full),
]
