from django import forms
from .models import Profesion, Profesor


class ProfesionForm(forms.ModelForm):
    class Meta:
        model= Profesion
        fields= ['nombre', 'anos']